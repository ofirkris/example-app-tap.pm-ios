//
//  AppDelegate.m
//  SimpleTapmeSDK
//
//  Created by Alexander Shvetsov on 09/01/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "AppDelegate.h"

#import <Tapme/Tapme.h>

static NSString *const kGameKey = @"gameKey"; // gameKey from Tapme SDK documentation

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self.window makeKeyAndVisible];
    
    UILocalNotification *localNotif = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    [self handlingLocalNotification:localNotif];
    
    return YES;
}


- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    //[self handlingLocalNotification:notification];
}


- (void)handlingLocalNotification:(UILocalNotification *)notif
{
    if ([notif.userInfo objectForKey:kGameKey])
    {
        [Tapme showGameWithSlug:[notif.userInfo objectForKey:kGameKey]
                          appID:kTapmeAppID
           presentingController:self.window.rootViewController
                       delegate:nil];
    }
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}


@end