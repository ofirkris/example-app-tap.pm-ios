//
//  AppDelegate.h
//  SimpleTapmeSDK
//
//  Created by Alexander Shvetsov on 09/01/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kTapmeAppID @"34d5ed1c_489f9829"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

