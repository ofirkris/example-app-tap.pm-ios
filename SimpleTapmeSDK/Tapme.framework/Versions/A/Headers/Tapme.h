//
//  Tapme.h
//  TapMe SDK
//
//  Created by Alexander Shvetsov on 22/06/2014.
//  Copyright (c) 2014 Smart Media. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol TMEventDelegate<NSObject>

@optional

/** Called when modal did appear */
- (void)TMModalDidAppear;

/** Called when modal did disappear */
- (void)TMModalDidDisappear;

/** Called when fail to load content */
- (void)TMConnectFailure:(NSError *)error;

/** Called when user converted successfully */
- (void)TMConversion;

/** Called when user gets points */
- (void)TMOffersComplete:(NSInteger)points;

@end

@interface Tapme : NSObject

/**
 * This method is called to show games.
 * @note Use the application id registered on the TapMe portal http://publishers.tap.pm/ .
 */
+ (void)showGamesWithAppID:(NSString *)appID
      presentingController:(UIViewController *)presentingController
                  delegate:(id<TMEventDelegate>)delegate;

/**
 * This method is called to show game by Slug.
 * @note Use the application id registered on the TapMe portal http://publishers.tap.pm/ .
 */
+ (void)showGameWithSlug:(NSString *)slug
                   appID:(NSString *)appID
    presentingController:(UIViewController *)presentingController
                delegate:(id<TMEventDelegate>)delegate;

/** This method is called to close Tapme Games */
+ (void)dismissTapmeModalAnimated:(BOOL)animated;

/**
 * Filtering games by special category.
 * @warning Need set before calling showGamesWithAppID:presentingController:delegate:
 */
+ (void)filterGamesByCategory:(NSString *)categoryGames;

/**
 * Background in main menu.
 * @warning Need set before calling showGamesWithAppID:presentingController:delegate:
 */
+ (void)setBackgroundHEXColor:(NSString *)backgroundHEXColor;

/**
 * Notifications offer to play a specific random game. 
 * Repeat every day. 
 * Contains the name of the game on a key @b gameKey @.
 * @code
 UILocalNotification *localNotif = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
 
 if ([localNotif.userInfo objectForKey:@"gameKey"])
 {
    // Show game use showGameWithSlug:appID:presentingController:delegate:
 }
 @endcode
 * @note Notifications will be set after receiving the list of games.
 * @warning Need set before calling showGamesWithAppID:presentingController:delegate:
 */
+ (void)setLocalNotificationsWithFireDate:(NSDate *)fireDate;

@end