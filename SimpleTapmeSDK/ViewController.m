//
//  ViewController.m
//  SimpleTapmeSDK
//
//  Created by Alexander Shvetsov on 09/01/2015.
//  Copyright (c) 2015 Yanpix - Shvetsov Alexander. All rights reserved.
//

#import "ViewController.h"

#import "TOWebViewController.h"
#import "UIAlertView+Blocks.h"
#import <Tapme/Tapme.h>
#import "AppDelegate.h"

@interface ViewController ()<TMEventDelegate, UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *slugField;
@property (strong, nonatomic) IBOutlet UILabel *scoreLabel;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSInteger score              = [userDefaults integerForKey:@"score"];
    
    self.scoreLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Score: %ld", @"Score: %ld"), (long)score];;
}


#pragma mark - IBAction

- (IBAction)moreGamesTapped:(id)sender
{
    // Installation of local notifications after 2 minutes.
    [Tapme setLocalNotificationsWithFireDate:[[NSDate date] dateByAddingTimeInterval:65]];
    
    [Tapme showGamesWithAppID:kTapmeAppID presentingController:self delegate:self];
}


- (IBAction)gameBySlugTapped:(id)sender
{
    NSString *slugString = [_slugField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if (slugString.length > 0)
    {
        [Tapme showGameWithSlug:slugString appID:kTapmeAppID presentingController:self delegate:self];
    }
    else {
        [_slugField becomeFirstResponder];
    }
}


- (IBAction)infoSlugTapped:(id)sender
{
    NSURL *url = [NSURL URLWithString:@"http://cdn.tap.pm/SDKcatalog.php"];
    
    TOWebViewController *webViewController = [[TOWebViewController alloc] initWithURL:url];
    
    [self presentViewController:[[UINavigationController alloc] initWithRootViewController:webViewController] animated:YES completion:nil];
}


#pragma mark - TMEventDelegate

- (void)TMConnectFailure:(NSError *)error
{
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error")
                                message:error.localizedDescription
                       cancelButtonItem:[RIButtonItem itemWithLabel:NSLocalizedString(@"OK", @"OK") action:nil]
                       otherButtonItems:nil] show];
}


- (void)TMOffersComplete:(NSInteger)points
{
    NSLog(@"Offers complete: %ld", (long)points);
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSInteger score = [userDefaults integerForKey:@"score"];
    
    score += points;
    
    [userDefaults setInteger:score forKey:@"score"];
    [userDefaults synchronize];
    
    self.scoreLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Score: %ld", @"Score: %ld"), (long)score];
}


#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    _slugField.text = [_slugField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    [textField resignFirstResponder];
    
    return YES;
}


@end